package com.gitlab.soshibby.issuereporter.messageappender;

import com.gitlab.soshibby.issuereporter.log.LogStatement;

import java.util.List;

public interface MessageAppender {
    void init(String config);
    String createMessageFrom(LogStatement logTrigger, List<LogStatement> logStatements);
}
